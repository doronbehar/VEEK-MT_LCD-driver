set_global_assignment -name VHDL_FILE design.vhd
set_global_assignment -name QIP_FILE PLL.qip -library LCD
set_global_assignment -name VERILOG_FILE ../i2c_touch_config.v -library LCD
set_global_assignment -name VHDL_FILE ../driver.vhd -library LCD
set_global_assignment -name VHDL_FILE ../lib/utils.vhd -library LCD
set_global_assignment -name VHDL_FILE ../lib/components.vhd -library LCD
set_global_assignment -name VHDL_FILE ../lib/font.vhd -library font
set_global_assignment -name VHDL_FILE ../char.ROM.vhd -library font
set_global_assignment -name VHDL_FILE ../char.RAM.vhd -library font
