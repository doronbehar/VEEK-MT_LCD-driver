# clock:
set_location_assignment PIN_Y2 -to CLK50MHZ

# LCD
set_location_assignment PIN_P28 -to LCD_B[0]
set_location_assignment PIN_P27 -to LCD_B[1]
set_location_assignment PIN_J24 -to LCD_B[2]
set_location_assignment PIN_J23 -to LCD_B[3]
set_location_assignment PIN_T26 -to LCD_B[4]
set_location_assignment PIN_T25 -to LCD_B[5]
set_location_assignment PIN_R26 -to LCD_B[6]
set_location_assignment PIN_R25 -to LCD_B[7]
set_location_assignment PIN_V24 -to LCD_DCLK
set_location_assignment PIN_H23 -to LCD_DE
set_location_assignment PIN_P21 -to LCD_DIM
set_location_assignment PIN_L23 -to LCD_DITH
set_location_assignment PIN_P26 -to LCD_G[0]
set_location_assignment PIN_P25 -to LCD_G[1]
set_location_assignment PIN_N26 -to LCD_G[2]
set_location_assignment PIN_N25 -to LCD_G[3]
set_location_assignment PIN_L22 -to LCD_G[4]
set_location_assignment PIN_L21 -to LCD_G[5]
set_location_assignment PIN_U26 -to LCD_G[6]
set_location_assignment PIN_U25 -to LCD_G[7]
set_location_assignment PIN_U22 -to LCD_HSD
set_location_assignment PIN_L24 -to LCD_MODE
set_location_assignment PIN_M25 -to LCD_POWER_CTL
set_location_assignment PIN_V28 -to LCD_R[0]
set_location_assignment PIN_V27 -to LCD_R[1]
set_location_assignment PIN_U28 -to LCD_R[2]
set_location_assignment PIN_U27 -to LCD_R[3]
set_location_assignment PIN_R28 -to LCD_R[4]
set_location_assignment PIN_R27 -to LCD_R[5]
set_location_assignment PIN_V26 -to LCD_R[6]
set_location_assignment PIN_V25 -to LCD_R[7]
set_location_assignment PIN_K22 -to LCD_RSTB
set_location_assignment PIN_H24 -to LCD_SHLR
set_location_assignment PIN_K21 -to LCD_UPDN
set_location_assignment PIN_V22 -to LCD_VSD

# keys:
set_location_assignment PIN_R24 -to KEY[3]
set_location_assignment PIN_N21 -to KEY[2]
set_location_assignment PIN_M21 -to KEY[1]
set_location_assignment PIN_M23 -to KEY[0]
