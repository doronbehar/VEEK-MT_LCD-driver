-- IMPORTANT: READ THIS IF YOU WANT TO USE THIS EXAMPLE AS
-- A REFERENCE FOR YOUR DESIGN:
-- =======================================================
-- I present to you a VHDL program that can print a multi line
-- messages on the LCD screen with a print procedure I wrote.
-- As you can see, I don't use NIOS or any other none VHDL stuff
-- In a real user interface like design, You might want to be
-- Able to print more than 2 sentences and print each one of
-- one after another after the user has made an input. I'm happy
-- to assure you that I wrote a design with such concept. It's
-- Available on github here:
-- https://github.com/Doron-Behar/RFID-entry-permissions-sys.
-- In that repository I created a bigger state machine to print
-- certain messages according to user input which includes as
-- well an RFID card reader. If you are struggling to find a
-- way to create such a system according to this example, check
-- out that repository of mine - It is much more maintained than
-- this one. NOTE: I use this repository as a sub module in the
-- other, big repo RFID-entry-permissions-sys.
--
-- I'll be more than happy to hear replies on these modules
-- I built at github - use the Issues tab.

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

library LCD;
use LCD.utils.all;
use LCD.components.all;
use LCD.main.all;

library font;
use font.utils.all;
use font.components.all;

entity example is
	port(
		CLK50MHZ		:in std_logic;
		LCD_R			:out std_logic_vector(7 downto 0);
		LCD_G			:out std_logic_vector(7 downto 0);
		LCD_B			:out std_logic_vector(7 downto 0);
		LCD_RSTB		:out std_logic;
		LCD_MODE		:out std_logic;
		LCD_DCLK		:out std_logic;
		LCD_HSD			:out std_logic;
		LCD_VSD			:out std_logic;
		LCD_SHLR		:out std_logic;
		LCD_UPDN		:out std_logic;
		LCD_DE			:out std_logic;
		LCD_DIM			:out std_logic;
		LCD_DITH		:out std_logic;
		LCD_POWER_CTL	:out std_logic;
		KEY				:in std_logic_vector(3 downto 0)
	);
end entity;


architecture arc of example is

--############################################################################
-------------------------------------Signals----------------------------------
--############################################################################

	signal reset:std_logic;
	signal char:font.utils.char_type;
	-- I named the following signal this way because it is used explicitly for
	-- displaying text.
	signal txt:font.utils.screen_type;
	signal clk33mhz:std_logic;

begin
	reset<=not KEY(0);

--############################################################################
--------------------------------------text------------------------------------
--############################################################################

	char_proc:process(txt.mode,clk33mhz) --where all the magic begins <3:
			-- There are 16 rows in a character and 8 columns in a character.
			-- for example:
			--	address number 0x0041 {'A' in ascii} contains the glyph:
			--	00 00 00 00 18 24 24 42 42 7E 42 42 42 42 00 00
		variable pc:integer range 0 to chars.portrait-1;
		variable pl:integer range 0 to lines.portrait-1;
		variable lc:integer range 0 to chars.landscape-1;
		variable ll:integer range 0 to lines.landscape-1;
	begin
		if rising_edge(clk33mhz) then
			case txt.mode is
				when portrait=>
							--	the display supposed to be:
							--		----->column            *  *
							--  |   - - - - - - - -      *        *
							--  |   - - - - - - - -     *  camera  *
							--  |   - - - - - - - -     *top--right*
							--  |   - - - - - - - -      *        *
							--  |   - - - # # - - -         *  *
							--  |   - - # - - # - -
							--  |   - - # - - # - -
							--  |   - # - - - - # -
							--  v   - # - - - - # -
							-- row  - # # # # # # -
							--		- # - - - - # -
							--		- # - - - - # -
							--		- # - - - - # -
							--		- # - - - - # -
							--		- - - - - - - -
							--		- - - - - - - -
						/*
						debuging version for calibration of the characters display while the position is fixed by 55 and 25
						char.print<=char.ROM.dataout(
							(conv_integer(txt.pixel.row(3 downto 0))-conv_integer(debuger(4 downto 1)))*8+
							(conv_integer(txt.pixel.column(2 downto 0))+conv_integer(debuger(7 downto 5)))mod 8
						);
						pc:=((conv_integer(not txt.pixel.column)+conv_integer(debuger(10 downto 8)))/8+
							55)mod(chars.portrait);
						pl:=((conv_integer(not txt.pixel.row)-conv_integer(debuger(14 downto 11)))/16+
							25)mod(lines.portrait);
						char.code<=char.portrait(index.portrait.c,index.portrait.l);
						*/
					char.print<=char.ROM.dataout(
						(conv_integer(txt.pixel.row(3 downto 0))-1)*8+
						(conv_integer(txt.pixel.column(2 downto 0))+0)mod 8
					);
					char.RAM.portrait.read.address.c<=((conv_integer(not txt.pixel.column)+0)/8+ 55)mod(chars.portrait);
					char.RAM.portrait.read.address.l<=((conv_integer(not txt.pixel.row)-2)/16+25)mod(lines.portrait);
				when landscape=>
							--	when mode is landscape the glyph should be displayed like that:
							--     *  *         ----->row
							--  *        *  |   - - - - - - - -
							-- *  camera  * |   - - - - - - - -
							-- * top-left * |   - - - - - - - -
							--  *        *  |   - - - - - - - -
							--     *  *     |   - - - # # - - -
							--              |   - - # - - # - -
							--              |   - - # - - # - -
							--              |   - # - - - - # -
							--              v   - # - - - - # -
							--            column- # # # # # # -
							--                  - # - - - - # -
							--                  - # - - - - # -
							--                  - # - - - - # -
							--                  - # - - - - # -
							--                  - - - - - - - -
							--                  - - - - - - - -
						/*
						debuging version for calibration of the characters display while the position is fixed by 49	and 29
						char.print<=char.ROM.dataout(
							(conv_integer(not txt.pixel.column(3 downto 0))+	conv_integer(debuger(4 downto 1)))*8+
							(conv_integer(txt.pixel.row(2 downto 0))+		conv_integer(debuger(7 downto 5)))mod 8
						);
						lc:=((conv_integer(not txt.pixel.row)+ conv_integer(debuger(10 downto 08)))/8+
							49)mod(chars.landscape);
						ll:=((conv_integer(txt.pixel.column)-	 conv_integer(debuger(14 downto 11)))/16+
							29)mod(lines.landscape);
						increment `not txt.pixel.row` to change characters' position to the left
						decrement `txt.pixel.column` to lower characters' position down
						*/
					char.print<=char.ROM.dataout(
						(conv_integer(not txt.pixel.column(3 downto 0))+1)*8+
						(conv_integer(txt.pixel.row(2 downto 0))+5)mod 8
					);
					char.RAM.landscape.read.address.c<=((conv_integer(not txt.pixel.row)+ 2)/8+49)mod(chars.landscape);
					char.RAM.landscape.read.address.l<=((conv_integer(txt.pixel.column)- 2)/16+29)mod(lines.landscape);
			end case;
			char.RAM.portrait.read.enable<='1';
		end if;
	end process;
	char_ROM_inst:font.components.char_ROM
		port map(
			address=>char.ROM.address,
			clock=>CLK50MHZ,
			q=>char.ROM.dataout
	);
	char_RAM_portrait_inst:font.components.char_RAM_portrait
		port map(
			clock		=>clk33mhz,
			data		=>char.RAM.portrait.write.data,
			rdaddress	=>char.RAM.portrait.read.address,
			rden		=>char.RAM.portrait.read.enable,
			wraddress	=>char.RAM.portrait.write.address,
			wren		=>char.RAM.portrait.write.enable,
			q			=>char.ROM.address
		);

--############################################################################
--------------------------------------main------------------------------------
--############################################################################

	main_proc:process(clk33mhz,reset)
		variable pl:integer range 0 to lines.portrait-1;
		variable pc:integer range 0 to chars.portrait-1;
		variable char2print:character;
		variable ready:boolean;
		procedure print(
			str:string(positive range<>)
		)is
		begin
			case pc is
				when 0 to str'length-1=>
					char2print:=str(pc+1);
				when others=>
					char2print:=' ';
			end case;
			if pc<str'length then
				pc:=pc+1;
				ready:=false;
				char.RAM.portrait.write.enable<='1';
			else
				pc:=0;
				char.RAM.portrait.write.enable<='0';
				ready:=true;
			end if;
		end procedure;
	begin
		char.RAM.portrait.write.data<=char2ascii(char2print);
		char.RAM.portrait.write.address.c<=pc;
		char.RAM.portrait.write.address.l<=pl;
		if reset='1' then
			txt.mode<=portrait;
			pl:=0;
			pc:=0;
		elsif rising_edge(clk33mhz) then
			case pl is
				when 0=>
					print("Hello world, My name is doron and I'm the developer");
				when 1=>
					print("Of this project.");
				when others=>
					-- Stop any printing procedures and stop incrementing pl with:
					ready:=false;
			end case;
			if ready then
				pl:=pl+1;
			end if;
		end if;
	end process;

--############################################################################
-------------------------------------Hardware---------------------------------
--############################################################################

	PLL_inst:LCD.main.PLL
		port map(
			areset	=>reset,
			inclk0	=>CLK50MHZ,
			c0		=>clk33mhz
	);
	LCD_driver_inst:LCD.main.driver
		port map(
			clk33mhz=>clk33mhz,
			reset=>reset,
			HSD=>LCD_HSD,
			VSD=>LCD_VSD,
			pixel=>txt.pixel
	);
	LCD_R			<=(others=>char.print);
	LCD_G			<=(others=>char.print);
	LCD_B			<=(others=>char.print);
	LCD_RSTB		<='1'; -- no reset
	LCD_MODE		<='0'; -- set up operation-mode:'sync';
	LCD_DCLK		<=clk33mhz;
	LCD_SHLR		<='0';
	LCD_UPDN		<='0';
	LCD_DE			<='1';
	LCD_DIM			<='1'; -- set Backlight
	LCD_DITH		<='1';
	LCD_POWER_CTL	<='1';

end arc;
