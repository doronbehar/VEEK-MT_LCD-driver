set_global_assignment -name FAMILY "Cyclone IV E"
set_global_assignment -name DEVICE EP4CE115F29C7
set_global_assignment -name TOP_LEVEL_ENTITY example
set_global_assignment -name VHDL_INPUT_VERSION VHDL_2008
set_global_assignment -name RESERVE_DATA0_AFTER_CONFIGURATION "AS INPUT TRI-STATED"
set_global_assignment -name CYCLONEII_RESERVE_NCEO_AFTER_CONFIGURATION "USE AS REGULAR IO"
set_global_assignment -name SDC_FILE "LCD-driver.sdc"
set_global_assignment -name ORIGINAL_QUARTUS_VERSION 15.1.2
set_global_assignment -name PROJECT_CREATION_TIME_DATE "19:47:31  APRIL 06, 2016"
set_global_assignment -name LAST_QUARTUS_VERSION 15.1.2
