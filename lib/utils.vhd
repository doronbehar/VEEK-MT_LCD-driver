library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

package utils is
	type horizontal_type is record
		P	:integer range 0 to 1056;
		PW	:integer range 0 to 30;
		BP	:integer range 0 to 16;
		FP	:integer range 0 to 210;
	end record;
	type vertical_type is record
		P	:integer range 0 to 525;
		PW	:integer range 0 to 13;
		BP	:integer range 0 to 10;
		FP	:integer range 0 to 22;
	end record;
	constant horizontal:horizontal_type:=(
		P=>1056,
		PW=>30,
		BP=>16,
		FP=>210
	);
	constant vertical:vertical_type:=(
		P=>525,
		PW=>13,
		BP=>10,
		FP=>22
	);
	type pixel_type is record
		row:std_logic_vector(10 downto 0);
		column:std_logic_vector(9 downto 0);
	end record;
	type screen_type is record
		red			:std_logic_vector(7 downto 0);
		green		:std_logic_vector(7 downto 0);
		blue		:std_logic_vector(7 downto 0);
		HSD			:std_logic;
		VSD			:std_logic;
		pixel		:pixel_type;
	end record;
end package;
