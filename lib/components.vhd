library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

library LCD;
use LCD.utils.all;

package components is
	component i2c_touch_config
		port(
			iCLK			:in std_logic;
			iRSTN			:in std_logic;
			iTRIG			:in std_logic;
			oREADY			:out std_logic;
			oREG_X1			:out std_logic_vector(9 downto 0);--1st touch row coordinate
			oREG_Y1			:out std_logic_vector(8 downto 0);--1st touch column coordinate
			oREG_X2			:out std_logic_vector(9 downto 0);--2nd touch row coordinate
			oREG_Y2			:out std_logic_vector(8 downto 0);--2nd touch column coordinate
			oREG_TOUCH_COUNT:out std_logic_vector(1 downto 0);--number of fingers touched
			oREG_GESTURE	:out std_logic_vector(7 downto 0);--touch gesture
				-- truth table for touch-gesture TG:
				--==================================
				--	ID[HEX]	|	Gesture
				-- ========================
				--	10		|	1 finger north
				--	12		|	1 finger north-east
				--	14		|	1 finger east
				--	16		|	1 finger south-east
				--	18		|	1 finger south
				--	1A		|	1 finger south-west
				--	1C		|	1 finger west
				--	1E		|	1 finger north-west
				--	28		|	1 finger rotates clockwise
				--	29		|	1 finger rotates anti-clockwise
				--	20		|	1 finger click
				--	22		|	1 finger double-click
				-- - - - - - - - - - - - - - - - - - -
				--	30		|	2 fingers north
				--	32		|	2 fingers north-east
				--	34		|	2 fingers east
				--	36		|	2 fingers south-east
				--	38		|	2 fingers south
				--	3A		|	2 fingers south-west
				--	3C		|	2 fingers west
				--	3E		|	2 fingers north-west
				--	40		|	2 fingers click
				--	48		|	zoom-in
				--	49		|	zoom-out
			I2C_SCLK		:out std_logic;
			I2C_SDAT		:inout std_logic
		);
	end component;
end package;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

library LCD;
use LCD.utils.all;

-- This package is for this demonstration purposes for this repo only.
	-- Why? Well, No matter what, if you are a power user and this repo is a
	-- submodule in your main project, It's better to implement in a project,
	-- 1 PLL for the whole of it as a single entity with all the clocks with
	-- it. So for example, If you have a project that contains other modules
	-- that need their clocks and you need a clock for my sub module as well,
	-- Use Quartus might get confused when compiling your code. According to
	-- my code writing principles, I encourage using libraries, packages and
	-- records. I generally use a package called `components` for my work
	-- library as well in my submodules. In my sub modules I have a package
	-- with the same name that is compiled with the sub modules' library name
	-- I specified in files.tcl. Therefor, Quartus might see your PLL component
	-- declaration in both `components` packages in the 2 separate packages
	-- (separated by the name of the library `work` and `<submodule>`). In this
	-- environment, it may complain that It has 1 too many component declaration
	-- for PLL. The solution is to use to packages in every submodule repo I
	-- create - I chose the name `main` because I feel it's the most pragmatic
	-- choice I could have done.
package main is
	component PLL
		port(
			areset	:in std_logic:='0';
			inclk0	:in std_logic:='0';
			c0		:out std_logic
		);
	end component;
	component driver
		port(
			clk33mhz	:in std_logic;
			reset		:in std_logic;
			HSD			:out std_logic;
			VSD			:out std_logic;
			pixel		:out pixel_type
			--rows are rows and columns are columns while the screen is standing with the camera at the top-right corner.
		);
	end component;
end package;
