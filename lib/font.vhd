library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

library LCD;
use LCD.utils.all;

package utils is
	type lines_type is record
		portrait:integer range 0 to (LCD.utils.horizontal.P-LCD.utils.horizontal.PW-LCD.utils.horizontal.BP-LCD.utils.horizontal.FP)/16;
		landscape:integer range 0 to (LCD.utils.vertical.P-LCD.utils.vertical.PW-LCD.utils.vertical.BP-LCD.utils.vertical.FP)/16;
	end record;
	type chars_type is record
		portrait:integer range 0 to (LCD.utils.vertical.P-LCD.utils.vertical.PW-LCD.utils.vertical.BP-LCD.utils.vertical.FP)/8;
		landscape:integer range 0 to (LCD.utils.horizontal.P-LCD.utils.horizontal.PW-LCD.utils.horizontal.BP-LCD.utils.horizontal.FP)/8;
	end record;
	constant lines:lines_type:=(
		portrait=>(LCD.utils.horizontal.P-LCD.utils.horizontal.PW-LCD.utils.horizontal.BP-LCD.utils.horizontal.FP)/16,
		--amount-of-lines --mode=portrait --not-in-pixels
		landscape=>(LCD.utils.vertical.P-LCD.utils.vertical.PW-LCD.utils.vertical.BP-LCD.utils.vertical.FP)/16
		--amount-of-lines --mode=landscape --not-in-pixels
	);
	constant chars:chars_type:=(
		portrait=>(LCD.utils.vertical.P-LCD.utils.vertical.PW-LCD.utils.vertical.BP-LCD.utils.vertical.FP)/8,
		--amount-of-characters --mode=portrait --not-in-pixels
		landscape=>(LCD.utils.horizontal.P-LCD.utils.horizontal.PW-LCD.utils.horizontal.BP-LCD.utils.horizontal.FP)/8
		--amount-of-characters landscape --not-in-pixels
	);
	subtype chr is std_logic_vector(6 downto 0);
	-- portrait char RAM types:
		-- Meant for RAM's input address as well:
	type char_RAM_portrait_address is record
		c:integer range 0 to chars.portrait-1;
		l:integer range 0 to lines.portrait-1;
	end record;
		-- Other types are meant for TLE.vhd signals
	type char_RAM_portrait_write_type is record
		enable:std_logic;
		data:chr;
		address:char_RAM_portrait_address;
	end record;
	type char_RAM_portrait_read_type is record
		enable:std_logic;
		address:char_RAM_portrait_address;
	end record;
	type char_RAM_portrait_type is record
		clear:std_logic;
		write:char_RAM_portrait_write_type;
		read:char_RAM_portrait_read_type;
	end record;
	-- landscape char RAM types:
		-- Meantfor RAM's input address as well:
	type char_RAM_landscape_address is record
		c:integer range 0 to chars.landscape-1;
		l:integer range 0 to lines.landscape-1;
	end record;
		-- Other types are meant for TLE.vhd signals
	type char_RAM_landscape_write_type is record
		enable:std_logic;
		data:chr;
		address:char_RAM_landscape_address;
	end record;
	type char_RAM_landscape_read_type is record
		enable:std_logic;
		address:char_RAM_landscape_address;
	end record;
	type char_RAM_landscape_type is record
		clear:std_logic;
		write:char_RAM_landscape_write_type;
		read:char_RAM_landscape_read_type;
	end record;
	-- global char types:
	type char_RAM_type is record
		portrait:char_RAM_portrait_type;
		landscape:char_RAM_landscape_type;
	end record;
	type char_ROM_type is record
		address:chr;
		dataout:std_logic_vector(127 downto 0);
	end record;
	type char_type is record
		print:std_logic;
		ROM:char_ROM_type;
		RAM:char_RAM_type;
		typed:character;
	end record;
	type display_mode_type is (landscape,portrait);
	type screen_type is record
		red			:std_logic_vector(7 downto 0);
		green		:std_logic_vector(7 downto 0);
		blue		:std_logic_vector(7 downto 0);
		HSD			:std_logic;
		VSD			:std_logic;
		pixel		:pixel_type;
		mode		:display_mode_type;
	end record;
	type cursor_landscape_type is record
		l:integer range 0 to lines.landscape-1;
		c:integer range 0 to chars.landscape-1;
	end record;
	type cursor_portrait_type is record
		l:integer range 0 to lines.portrait-1;
		c:integer range 0 to chars.portrait-1;
	end record;
	type cursor_type is record
		landscape:cursor_landscape_type;
		portrait:cursor_portrait_type;
		print:std_logic;
		clk:std_logic;
		blink:boolean;
		show:boolean;
	end record;
	type C_wide_type is record
		small:integer range 0 to 1056;
		big:integer range 0 to 1056;
	end record;
	type C_narrow_type is record
		small:integer range 0 to 525;
		big:integer range 0 to 525;
	end record;
	type C_landscape_type is record
		l:C_narrow_type;
		c:C_wide_type;
	end record;
	type C_portrait_type is record
		l:C_wide_type;
		c:C_narrow_type;
	end record;
	type C_type is record
		landscape:C_landscape_type;
		portrait:C_portrait_type;
	end record;
	type keyboard_layout_portrait_chars_type is array(3 downto 0) of string(1 to chars.portrait);
	type keyboard_layout_landscape_chars_type is array(3 downto 0) of string(1 to chars.landscape);
	type keyboard_layout_portrait_numbers_type is array(2 downto 0) of string(1 to chars.portrait);
	type keyboard_layout_landscape_numbers_type is array(2 downto 0) of string(1 to chars.landscape);
	type keyboard_layout_portrait_type is record
		uppercase:keyboard_layout_portrait_chars_type;
		lowercase:keyboard_layout_portrait_chars_type;
		signs:keyboard_layout_portrait_chars_type;
		numbers:keyboard_layout_portrait_numbers_type;
	end record;
	type keyboard_layout_landscape_type is record
		uppercase:keyboard_layout_landscape_chars_type;
		lowercase:keyboard_layout_landscape_chars_type;
		signs:keyboard_layout_landscape_chars_type;
		numbers:keyboard_layout_landscape_numbers_type;
	end record;
	type keyboard_layout_type is record
		portrait:keyboard_layout_portrait_type;
		landscape:keyboard_layout_landscape_type;
	end record;
	constant keyboard_layout:keyboard_layout_type:=(
		portrait=>(
			uppercase=>(
				3=>"  Q     W     E     R     T     Y     U     I     O     P   ",
				2=>"     A     S     D     F     G     H     J     K     L      ",
				1=>"  "&si&"        Z     X     C     V     B     N     M     "&del&"      ",
				0=>" 12#             ___________________            .    "&nul&"      "
			),lowercase=>(
				3=>"  q     w     e     r     t     y     u     i     o     p   ",
				2=>"     a     s     d     f     g     h     j     k     l      ",
				1=>"  "&si&"        z     x     c     v     b     n     m     "&del&"      ",
				0=>" 12#             ___________________            .    "&nul&"      "
			),signs=>(
				3=>"  1     2     3     4     5     6     7     8     9     0   ",
				2=>"     !     @     #     $     %     &     +     ?     /      ",
				1=>"     *     _     "&'"'&"     (     )     -     :     :     "&del&"      ",
				0=>" 12#             ___________________            .    "&nul&"      "
			),numbers=>(
				2=>"                       1     2     3                        ",
				1=>"                       4     5     6                        ",
				0=>"                       7     8     9                        "
			)
		),landscape=>(
			uppercase=>(
				3=>"                                                                                                    ",
				2=>"                                                                                                    ",
				1=>"                                                                                                    ",
				0=>"                                                                                                    "
			),lowercase=>(
				3=>"                                                                                                    ",
				2=>"                                                                                                    ",
				1=>"                                                                                                    ",
				0=>"                                                                                                    "
			),signs=>(
				3=>"                                                                                                    ",
				2=>"                                                                                                    ",
				1=>"                                                                                                    ",
				0=>"                                                                                                    "
			),numbers=>(
				2=>"                                           1     2     3                                            ",
				1=>"                                           4     5     6                                            ",
				0=>"                                           7     8     9                                            "
			)
		)
	);
	type keyboard_current_type is (lowercase,uppercase,signs);
	type keyboard_type is record
		current:keyboard_current_type;
		--updating:boolean;
		--finish_update:boolean;
	end record;
	function char2ascii(char:character)return std_logic_vector;
	function digit(dig:integer range 0 to 31;int:integer) return integer;
	function integer2char(int:integer) return character;
	function integer2string(int:integer;len:integer) return string;
	function logic2char(logic:std_logic) return character;
	function logic2string(logic:std_logic_vector(39 downto 0)) return string;
end package;

package body utils is
	function char2ascii(
		char:character
	)return std_logic_vector is
	begin
		case char is
		when'0'		=>return conv_std_logic_vector(16	,7);
		when'1'		=>return conv_std_logic_vector(17	,7);
		when'2'		=>return conv_std_logic_vector(18	,7);
		when'3'		=>return conv_std_logic_vector(19	,7);
		when'4'		=>return conv_std_logic_vector(20	,7);
		when'5'		=>return conv_std_logic_vector(21	,7);
		when'6'		=>return conv_std_logic_vector(22	,7);
		when'7'		=>return conv_std_logic_vector(23	,7);
		when'8'		=>return conv_std_logic_vector(24	,7);
		when'9'		=>return conv_std_logic_vector(25	,7);
		when':'		=>return conv_std_logic_vector(26	,7);
		when';'		=>return conv_std_logic_vector(27	,7);
		when'<'		=>return conv_std_logic_vector(28	,7);
		when'='		=>return conv_std_logic_vector(29	,7);
		when'>'		=>return conv_std_logic_vector(30	,7);
		when'?'		=>return conv_std_logic_vector(31	,7);
		when'@'		=>return conv_std_logic_vector(32	,7);
		when'A'		=>return conv_std_logic_vector(33	,7);
		when'B'		=>return conv_std_logic_vector(34	,7);
		when'C'		=>return conv_std_logic_vector(35	,7);
		when'D'		=>return conv_std_logic_vector(36	,7);
		when'E'		=>return conv_std_logic_vector(37	,7);
		when'F'		=>return conv_std_logic_vector(38	,7);
		when'G'		=>return conv_std_logic_vector(39	,7);
		when'H'		=>return conv_std_logic_vector(40	,7);
		when'I'		=>return conv_std_logic_vector(41	,7);
		when'J'		=>return conv_std_logic_vector(42	,7);
		when'K'		=>return conv_std_logic_vector(43	,7);
		when'L'		=>return conv_std_logic_vector(44	,7);
		when'M'		=>return conv_std_logic_vector(45	,7);
		when'N'		=>return conv_std_logic_vector(46	,7);
		when'O'		=>return conv_std_logic_vector(47	,7);
		when'P'		=>return conv_std_logic_vector(48	,7);
		when'Q'		=>return conv_std_logic_vector(49	,7);
		when'R'		=>return conv_std_logic_vector(50	,7);
		when'S'		=>return conv_std_logic_vector(51	,7);
		when'T'		=>return conv_std_logic_vector(52	,7);
		when'U'		=>return conv_std_logic_vector(53	,7);
		when'V'		=>return conv_std_logic_vector(54	,7);
		when'W'		=>return conv_std_logic_vector(55	,7);
		when'X'		=>return conv_std_logic_vector(56	,7);
		when'Y'		=>return conv_std_logic_vector(57	,7);
		when'Z'		=>return conv_std_logic_vector(58	,7);
		when'['		=>return conv_std_logic_vector(59	,7);
		when'\'		=>return conv_std_logic_vector(60	,7);
		when']'		=>return conv_std_logic_vector(61	,7);
		when'^'		=>return conv_std_logic_vector(62	,7);
		when'_'		=>return conv_std_logic_vector(63	,7);
		when'`'		=>return conv_std_logic_vector(64	,7);
		when'a'		=>return conv_std_logic_vector(65	,7);
		when'b'		=>return conv_std_logic_vector(66	,7);
		when'c'		=>return conv_std_logic_vector(67	,7);
		when'd'		=>return conv_std_logic_vector(68	,7);
		when'e'		=>return conv_std_logic_vector(69	,7);
		when'f'		=>return conv_std_logic_vector(70	,7);
		when'g'		=>return conv_std_logic_vector(71	,7);
		when'h'		=>return conv_std_logic_vector(72	,7);
		when'i'		=>return conv_std_logic_vector(73	,7);
		when'j'		=>return conv_std_logic_vector(74	,7);
		when'k'		=>return conv_std_logic_vector(75	,7);
		when'l'		=>return conv_std_logic_vector(76	,7);
		when'm'		=>return conv_std_logic_vector(77	,7);
		when'n'		=>return conv_std_logic_vector(78	,7);
		when'o'		=>return conv_std_logic_vector(79	,7);
		when'p'		=>return conv_std_logic_vector(80	,7);
		when'q'		=>return conv_std_logic_vector(81	,7);
		when'r'		=>return conv_std_logic_vector(82	,7);
		when's'		=>return conv_std_logic_vector(83	,7);
		when't'		=>return conv_std_logic_vector(84	,7);
		when'u'		=>return conv_std_logic_vector(85	,7);
		when'v'		=>return conv_std_logic_vector(86	,7);
		when'w'		=>return conv_std_logic_vector(87	,7);
		when'x'		=>return conv_std_logic_vector(88	,7);
		when'y'		=>return conv_std_logic_vector(89	,7);
		when'z'		=>return conv_std_logic_vector(90	,7);
		when'{'		=>return conv_std_logic_vector(91	,7);
		when'|'		=>return conv_std_logic_vector(92	,7);
		when'}'		=>return conv_std_logic_vector(93	,7);
		when'~'		=>return conv_std_logic_vector(94	,7);
		when' '		=>return conv_std_logic_vector(0	,7);
		when'!'		=>return conv_std_logic_vector(1	,7);
		when'&'		=>return conv_std_logic_vector(6	,7);
		when'''		=>return conv_std_logic_vector(7	,7);
		when'('		=>return conv_std_logic_vector(8	,7);
		when')'		=>return conv_std_logic_vector(9	,7);
		when'*'		=>return conv_std_logic_vector(10	,7);
		when'+'		=>return conv_std_logic_vector(11	,7);
		when','		=>return conv_std_logic_vector(12	,7);
		when'-'		=>return conv_std_logic_vector(13	,7);
		when'.'		=>return conv_std_logic_vector(14	,7);
		when'/'		=>return conv_std_logic_vector(15	,7);
		when'"'		=>return conv_std_logic_vector(2	,7);
		when'#'		=>return conv_std_logic_vector(3	,7);
		when'$'		=>return conv_std_logic_vector(4	,7);
		when'%'		=>return conv_std_logic_vector(5	,7);--%
		when si		=>return conv_std_logic_vector(122	,7);--%"ᛏ"%
		when dc1	=>return conv_std_logic_vector(123	,7);--%"◖"%
		when dc2	=>return conv_std_logic_vector(124	,7);--%"◗"%
		when dc3	=>return conv_std_logic_vector(125	,7);--%"♮"%
		when del	=>return conv_std_logic_vector(126	,7);--%"˿"%
		when nul	=>return conv_std_logic_vector(127	,7);--%"˩"%
		when c128	=>return conv_std_logic_vector(95	,7);--%"א"%
		when c129	=>return conv_std_logic_vector(96	,7);--%"ב"%
		when c130	=>return conv_std_logic_vector(97	,7);--%"ג"%
		when c131	=>return conv_std_logic_vector(98	,7);--%"ד"%
		when c132	=>return conv_std_logic_vector(99	,7);--%"ה"%
		when c133	=>return conv_std_logic_vector(100	,7);--%"ו"%
		when c134	=>return conv_std_logic_vector(101	,7);--%"ז"%
		when c135	=>return conv_std_logic_vector(102	,7);--%"ח"%
		when c136	=>return conv_std_logic_vector(103	,7);--%"ט"%
		when c137	=>return conv_std_logic_vector(104	,7);--%"י"%
		when c138	=>return conv_std_logic_vector(105	,7);--%"ך"%
		when c139	=>return conv_std_logic_vector(106	,7);--%"כ"%
		when c140	=>return conv_std_logic_vector(107	,7);--%"ל"%
		when c141	=>return conv_std_logic_vector(108	,7);--%"ם"%
		when c142	=>return conv_std_logic_vector(109	,7);--%"מ"%
		when c143	=>return conv_std_logic_vector(110	,7);--%"ן"%
		when c144	=>return conv_std_logic_vector(111	,7);--%"נ"%
		when c145	=>return conv_std_logic_vector(112	,7);--%"ס"%
		when c146	=>return conv_std_logic_vector(113	,7);--%"ע"%
		when c147	=>return conv_std_logic_vector(114	,7);--%"ף"%
		when c148	=>return conv_std_logic_vector(115	,7);--%"פ"%
		when c149	=>return conv_std_logic_vector(116	,7);--%"ץ"%
		when c150	=>return conv_std_logic_vector(117	,7);--%"צ"%
		when c151	=>return conv_std_logic_vector(118	,7);--%"ק"%
		when c152	=>return conv_std_logic_vector(119	,7);--%"ר"%
		when c153	=>return conv_std_logic_vector(120	,7);--%"ש"%
		when others	=>return conv_std_logic_vector(121	,7);--%"ת"%
		end case;
	end function char2ascii;
	function digit(
		dig:integer range 0 to 31;
		int:integer
	)return integer is
	begin
		return ((int/(10**dig))mod 10);
	end function digit;
	function integer2char(int: integer) return character is
		-- converts an integer into a character
		-- for 0 to 9 the obvious mapping is used, higher
		-- values are mapped to the characters A-Z
		-- (this is usefull for systems with base > 10)
		-- (adapted from Steve Vogwell's posting in comp.lang.vhdl)
	begin
		case int is
			when	0	=>return	'0';
			when	1	=>return	'1';
			when	2	=>return	'2';
			when	3	=>return	'3';
			when	4	=>return	'4';
			when	5	=>return	'5';
			when	6	=>return	'6';
			when	7	=>return	'7';
			when	8	=>return	'8';
			when	9	=>return	'9';
			when	10	=>return	'A';
			when	11	=>return	'B';
			when	12	=>return	'C';
			when	13	=>return	'D';
			when	14	=>return	'E';
			when	15	=>return	'F';
			when	16	=>return	'G';
			when	17	=>return	'H';
			when	18	=>return	'I';
			when	19	=>return	'J';
			when	20	=>return	'K';
			when	21	=>return	'L';
			when	22	=>return	'M';
			when	23	=>return	'N';
			when	24	=>return	'O';
			when	25	=>return	'P';
			when	26	=>return	'Q';
			when	27	=>return	'R';
			when	28	=>return	'S';
			when	29	=>return	'T';
			when	30	=>return	'U';
			when	31	=>return	'V';
			when	32	=>return	'W';
			when	33	=>return	'X';
			when	34	=>return	'Y';
			when	35	=>return	'Z';
			when others	=>return	'?';
		end case;
	end function;
	function integer2string(
		int:integer;
		len:integer
	)return string is
		variable str:string(32 downto 1);
	begin
		for i in 1 to len loop
			str(i):=integer2char(digit(i-1,int));
		end loop;
		return str(len downto 1);
	end function;
	function logic2char(
		logic:std_logic
	)return character is
	begin
		case logic is
			when 'U'=>return 'U';
			when 'X'=>return 'X';
			when '0'=>return '0';
			when '1'=>return '1';
			when 'Z'=>return 'Z';
			when 'W'=>return 'W';
			when 'L'=>return 'L';
			when 'H'=>return 'H';
			when '-'=>return '-';
		end case;
	end function;
	function logic2string(logic:std_logic_vector(39 downto 0)) return string is
		variable str:string(40 downto 1);
	begin
		for i in 1 to 40 loop
			str(i):=logic2char(logic(i-1));
		end loop;
		return str;
	end function;
end package body;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

library LCD;
use LCD.utils.all;

library font;
use font.utils.all;

package components is
	component char_ROM
		port(
			address	:in chr;
			clock	:in std_logic:= '1';
			q		:out std_logic_vector(127 downto 0)
		);
	end component;
	component char_RAM_portrait
		port (
			clock		:in std_logic;
			data		:in chr;
			rdaddress	:in char_RAM_portrait_address;
			rden		:in std_logic;
			wraddress	:in char_RAM_portrait_address;
			wren		:in std_logic;
			q			:out chr
		);
	end component;
end package;
