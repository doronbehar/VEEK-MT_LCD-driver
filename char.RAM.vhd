library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

library LCD;
use LCD.utils.all;

library font;
use font.utils.all;

entity char_RAM_portrait is
	port (
		clock		:in std_logic;
		data		:in chr;
		rdaddress	:in char_RAM_portrait_address;
		rden		:in std_logic;
		wraddress	:in char_RAM_portrait_address;
		wren		:in std_logic;
		q			:out chr
	);
end entity;

architecture arc of char_RAM_portrait is
	type RAM_type is array(chars.portrait-1 downto 0,lines.portrait-1 downto 0)of chr;
	signal RAM:RAM_type;
begin
	process(clock)
	begin
		if rising_edge(clock) then
			if wren='1' then
				RAM(wraddress.c,wraddress.l)<=data;
			end if;
			if rden='1' then
				q<=RAM(rdaddress.c,rdaddress.l);
			else
				q<=(others=>'0');
			end if;
		end if;
	end process;
end arc;
