library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.std_logic_arith.all;

library LCD;
use LCD.utils.all;

entity driver is
	port(
		clk33mhz	:in std_logic;
		reset		:in std_logic;
		HSD			:out std_logic;
		VSD			:out std_logic;
		pixel		:out pixel_type
		--rows are rows and columns are columns while the screen is standing with the camera at the top-right corner.
);
end entity;

architecture arc of driver is
	signal counter		:pixel_type;
		--horizontal while the screen is standing with the camera at the top-left corner.
		--                                              *  *
		--                                           *        *
		--                                          *  camera  *
		--                                          *          *
		--                                           *        *
		--                                              *  *
		--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		--|                 %    ^    %                |
		--|                row  HFP  210               |
		--|                 %    ^    %                |
		--|           !--------------------------!     |
		--|           !portrait                 l!     |
		--|           !                         a!     |
		--|           !                         n!     |
		--|           !                         d!     |
		--|%----23---%!                         s!%%22%|
		--|<-VPW+VBP-<!                         c!<VFP<|
		--|%%column%%%!                         a!     |
		--|           !                         p!     |
		--|           !                         e!     |
		--|           !                          !     |
		--|           !                          !     |
		--|           !                          !     |
		--|           !                          !     |
		--|           !                          !     |
		--|           !                          !     |
		--|           !                          !     |
		--|           !                          !     |
		--|           !                     hello!     |
		--|           !--------------------------!     |
		--|                %       ^     %             |
		--|                %       |     |             |
		--|               row   HBP+HPW  46            |
		--|                %       |     |             |
		--|                %       ^     %             |
		--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
begin
	--up is up and left is left when the screen is standing whith the camera at the top-right corner.
	process(clk33mhz,reset)
	begin
		if reset='1' then
			counter.row<=(others=>'0');
			counter.column<=(others=>'0');
		elsif rising_edge(clk33mhz) then
			if counter.row<LCD.utils.horizontal.P-1 then
				counter.row<=counter.row+1;
			else
				counter.row<=(others=>'0');
			end if;
			if counter.column<LCD.utils.vertical.P-1 and counter.row=LCD.utils.horizontal.P-1 then
				counter.column<=counter.column+1;
			elsif counter.row=LCD.utils.horizontal.P-1 then
				counter.column<=(others=>'0');
			end if;
		end if;
		if counter.row<LCD.utils.horizontal.BP+LCD.utils.horizontal.PW then --for the default mode='0'
			HSD<='0';
		else
			HSD<='1';
		end if;
		if counter.column<LCD.utils.vertical.BP+LCD.utils.vertical.PW then --for the default mode='0'
			VSD<='0';
		else
			VSD<='1';
		end if;
	end process;
	pixel<=counter;
end arc;
