LCD-driver for VEEK-MT by Terasic
=================================
## Intro
In this project you will find essentially 2 things: **Above all**, you can be use it as a sub module to give you a straight forward, straight away and reliable driver that was tested and it works specifically for the _VEEK-MT_ LCD touch screen. **In addition**, I created a simple to use module with great API, that prints text on the display. In fact, I believe it shouldn't be that hard to use this project's example (check out [`example/design.vhd`](https://github.com/Doron-Behar/VEEK-MT_LCD-driver/blob/master/example/design.vhd)) as a guide for how to print **characters and full sentences extremely easily and efficiently** even when working with a different display. I tried to calculate the display of the characters in a generic way so it will be easy to import it to another project with different parameters.

## The Hardware
This project is designed Specifically for the _Video and Embedded Evaluation Kit - Multi-touch_ by Terasic. I tried to create this driver as generic as possible so it may set an example for users who write drivers for other displays. If this is your first time you are about to use some kind of display with FPGA, (if it's VGA sync module exercise for school or something similar there are many good VGA modules out there) You can use this module as an example or even try it as it is and tell me what are the results.

## Using this driver for other displays
What you need to know beforehand is what defined in [`lib/utils.vhd`](https://github.com/Doron-Behar/VEEK-MT_LCD-driver/blob/master/lib/utils.vhd):
```vhdl
	constant horizontal:horizontal_type:=(
		P=>1056,
		PW=>30,
		BP=>16,
		FP=>210
	);
	constant vertical:vertical_type:=(
		P=>525,
		PW=>13,
		BP=>10,
		FP=>22
	);
```
This section is defining the parameters for the display that are inherited by it's Physical dimensions and it can all be found in the original documentation.
**NOTE:** Check out [`lib/utils.vhd`](https://github.com/Doron-Behar/VEEK-MT_LCD-driver/blob/master/lib/utils.vhd) and you will see I defined the constants above as records beforehand so it will be easy for advanced users to redefine these variables in a way it's easy to use them later in the entities and it's easy to view it and change it. See more details under [CONTRIBUTING.md](https://github.com/Doron-Behar/VEEK-MT_LCD-driver/blob/master/CONTRIBUTING.md).

## License
Lets say I'm not an expert for licensing, So I'll make it simple by saying I'm a man of free software and I encourage collaboration with `git` and `github`. If you want something more official to follow after, you can check out The [GPL version 2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt) and look at it as the license for this project.

## Contributing
Read [CONTRIBUTING.md](https://github.com/Doron-Behar/VEEK-MT_LCD-driver/blob/master/CONTRIBUTING.md)

## Hardware Documentation
Documentation for the display is in the user manual and data sheet attached to this repository:

- [User manual](manual.pdf)
- [Data sheet](datasheet.pdf)

In order to use the display, you have to send signals to the display's pins with exactly the right timing. All according to the timing specification described in the **User manual table 3-1, page 17** and on the **datasheet section 8.3, page 15**.

## Common Problems specifically when using the VEEK-MT with Quartus.

#### Pin Assignments Errors
While compiling the code in older versions of Quartus, one may encounter the following error in the "Fitter (place&route)" process as part of the full compilation:

##### Error Message
```
Error (176310): Can't place multiple pins assigned to pin location Pin_P28 (IOPAD_X115_Y43_N7)
	Info (176311): Pin LCD_B[0] is assigned to pin location Pin_P28 (IOPAD_X115_Y43_N7)
	Info (176311): Pin ~ALTERA_nCEO~ is assigned to pin location Pin_P28 (IOPAD_X115_Y43_N7)
```

##### Solution
- Go to: **'Assigments'**=>**'Device'**=>**'Device and Pin Options'**=>**'Category'**=>**'Dual-Purpose Pins'**=>**'nCEO'**=>**change to 'Use as regular I/0'**
- Hit **'OK'**
- Run Full Compilation and error fixed

Source: http://www.fpga-talk.de/forum/viewtopic.php?f=8&t=202
