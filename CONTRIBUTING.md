# Contributing
first of all, I encourage anyone who wants to talk about and contribute to my repositories. **Unfortunately**, It's not very common among HDL developers to use git and github :(( If you don't already, I suggest you start using git and realize it's an extremely useful tool for project management especially with languages like VHDL.

You might want to take a look at the example of a CONTRIBUTION statement from one of the most frequently used Vim plugin managers and one of my favorite github users:
[https://github.com/tpope/vim-pathogen/blob/master/CONTRIBUTING.markdown](https://github.com/tpope/vim-pathogen/blob/master/CONTRIBUTING.markdown). If you'll read tpope's 1st paragraph in his statement, you'll get the sense of what I'm trying to say. I have a strict way for writing VHDL code and I stand for it like rules in my repositories:

## Use Quartus' tcl scripts.
This is probably the most important part in my work flow. Let's take a look at what You should know before using the tcl scripts. I'll divide this section into 2 parts:
- The problem with most people's Work flow - The GUI vs The CLI.
- How to use the tcl in the most comfortable way for you and for git.

#### The Quartus GUI is good but it's not perfect.
There are many features I feel they lack for me with this software but it's not so bad. In addition, It's probably the only choice you will feel the most comfortable when developing with Altera devices. The most relevant issues with The Quartus GUI is that it's hard to use if you are using a version control system like git. In my 1st projects when I started to use git, I've found out that It's pretty much impossible to track the `<project>.qsf` file - It changes all the time. If you don't already know, when you create a project with Quartus, this file is storing important settings for your project that you might want to keep track of. The `.qsf` file includes:

- The files that are included in the project.
- Pin location assignments.
- Other general settings you might want to keep track of.

The documentation for using the quartus in the CLI is not very fancy So I'll cover the way I use it in my projects and I think it could be everything you need to know when using the Quartus in a scripting way. Yet, if you really want to master it, I would recommend reading [https://www.altera.com/content/dam/altera-www/global/en_US/pdfs/literature/manual/tclscriptrefmnl.pdf](https://www.altera.com/content/dam/altera-www/global/en_US/pdfs/literature/manual/tclscriptrefmnl.pdf) because it's pretty much the only documentation out there but it's the most official one to read although it's not very exiting.

#### What tcl scripts I use and how to use them.
Integrating git with the Quartus' project files is done by adding tcl scripts you want git to track to the `qsf`. **Usually**, I add the following to every `qsf` file I create:
```tcl
source pins.tcl
source files.tcl
source settings.tcl
```
As you might guess, I tell git to track all tcl files I specify in the `qsf` file and I tell git to ignore `*qsf` in `.gitignore`. In `pins.tcl` I add pin assignments as they are usually written in the `qsf` and the same with `files.tcl`. In `settings.tcl` on the other hand, I put general settings I extract from the `qsf` file. **It's very important to keep track after the `.qsf` that it doesn't have double assignments from the tcl scripts and it self.** This part is very important - If you use the GUI a lot, (for adding files and pin assignments for example) you have to learn to use the tcl scripts. The tcl scripts + git = is the perfect combination of an advanced work flow with Quartus' tools.

## Use libraries
As part of learning to use the tcl scripts, I like using **libraries** in my work. Many times, during The time you work on a project, You might find a github repository with a module you find useful for the project. In that case, it would be much easier and more advanced to use that module as a git sub module and add the proper files in certain libraries.

For example let's say you want to use this repo as a library for your project. We'll call this library `LCD` and the git sub module it self will be stored in directory `LCD/`. You will need to do the following:

- Add the repo as a sub module:
```
git submodule add <URL> LCD # the last argument is the name of the directory the sub module will be downloaded to.
```
- Open `LCD/files.tcl` and copy it's content to **your project's** `files.tcl`
- All that is left is to edit the path for the files in your project's `files.tcl`. This is because the previous path from `LCD/files.tcl` was relevant for that directory So you just need to add `LCD/` before every path.

**NOTE:** You can see that for every file you specify in your `files.tcl`, you now have the argument `-library LCD` at the end. This argument tells Quartus that when compiling your code, If you write  for example:
```
library LCD;
use LCD.utils.all;
```
It means you are using the `utils` package from library `LCD` and the package can be declared within any file that you have specified in `files.tcl` and with the argument `-library LCD`.

## Use the makefile
Although this is not a must, I believe you'll find my makefiles are very useful in the work flow. As I talked about it earlier, It's pretty hard to remember all the Options for the Quartus scripting language and especially with a horrible documentation like they have. That's why I almost always have a makefile in my projects. It's also a good place to look at some of the projects settings (besides `settings.tcl`)

Here is a makefile for example:
```makefile
# project-name
P=LCD-driver
# top-level-entity
TLE=example
# device selected for programming
DEV=EP4CE115F29C7
# family of the device
F="Cyclone IV E"
# custom FLAGS - mainly for setting the 64 bit flag or not:
FLAGS=--64bit

all: compile program
compile:
	quartus_sh ${FLAGS} --flow compile ${P}
project:
	quartus_sh ${FLAGS} --tcl_eval project_new -f ${F} -overwrite -p ${DEV} ${P}
program:
	quartus_pgm ${FLAGS} -c USB-Blaster ${P}.cdf || quartus_pgm ${FLAGS} -c USB-Blaster --debug -m JTAG -o P\;${P}.sof
RTL:
	qnui ${FLAGS} ${P}
symbol:
	quartus_map ${FLAGS} ${P} --generate_symbol=${d}
analysis:
	quartus_map ${FLAGS} ${P}
gui:
	quartus gui ${P}.qpf || quartus ${FLAGS} ${P}.qpf || ${QUARTUS_BIN}/quartus ${FLAGS} ${P}.qpf
```
If you don't know how makefiles work, It's quite simple if you are using it for simple things like this. You can divide the file into 2 sections:
- Variables
- targets/scripts

The 1st part is where you define some variables for the scripts themselves. For example, this makefile is an exact copy of [example/makefile](https://github.com/Doron-Behar/VEEK-MT_LCD-driver/blob/master/example/makefile). You need to tell Quartus the device you plan to work with when you create the project. So if you run `make project` The project target is being read and You essentially run the command:
```sh
quartus_sh --64bit --tcl_eval project_new -f "Cyclone IV E" -overwrite -p EP4CE115F29C7 LCD-driver
```
- `--64bit` is just a flag for compiling a bit faster if you run a 64bit machine. (you can disable it if you are running a 32bit machine by using `FLAGS=` (empty) at the variables section of the makefile)
- `"Cyclone IV E"` is the family of the devices you are working with.
- `EP4CE115F29C7` is the name of the specific device you are working with.
- `LCD-driver` is the name of the project.

If you understood this, you should figure out your selves the rest of the `makefile`.


## Important Notes
- Try to use the GUI is less as possible.
- Especially don't use the GUI for editing the files of the project - Edit `files.tcl`.
- Don't use the GUI to add pin assignments - These will be added to `<project>.qsf` and not to `pins.tcl` and Quartus will usually prefer the `qsf` file over your tcl script.
- Don't Edit `qsf` or any other `tcl` script during a compilation - It will write all the settings in your scripts to the `qsf` file and You will end up having to delete all of those lines afterwards after your compilation have stopped.
